use std::{
    collections::HashMap,
    io::{BufRead, BufReader, Read},
    net::TcpStream,
};

use super::{cookies::CookieJar, http_version::HttpVersion, status_code::StatusCode};

#[derive(Debug)]
pub struct Response {
    pub http_version: HttpVersion,
    pub status_code: StatusCode,
    pub headers: HashMap<String, String>,
    pub cookies: CookieJar,
    pub body: Option<String>,
}

impl Response {
    pub(crate) fn parse(stream: &mut TcpStream) -> Self {
        let mut buf = BufReader::new(stream);

        let mut status_line = String::new();

        buf.read_line(&mut status_line).unwrap();

        let status_line = status_line.trim();
        let Some((http_version, status_line_without_http_version)) = status_line.split_once(' ') else {
            panic!("Invalid response status line `{}`", status_line);
        };
        let http_version = HttpVersion::parse(http_version);
        let Some((status_code, _)) = status_line_without_http_version.split_once(' ') else {
            panic!("Invalid response status line `{}`", status_line);
        };
        let status_code: u16 = status_code.parse().unwrap();
        let status_code: StatusCode = unsafe { std::mem::transmute(status_code) };

        let mut headers = HashMap::new();

        loop {
            let mut line = String::new();
            buf.read_line(&mut line).unwrap();
            let line = line.trim();
            if line.is_empty() {
                break;
            }
            let (header_name, header_value) = line.split_once(':').unwrap();
            let (header_name, header_value) = (
                header_name.trim().to_lowercase().to_string(),
                header_value.trim().to_string(),
            );
            headers.insert(header_name, header_value);
        }

        let content_length = headers
            .get("content-length")
            .map(|value| value.parse().unwrap())
            .unwrap_or(0usize);

        let body = if content_length == 0 {
            None
        } else {
            let mut body = vec![0; content_length];
            buf.read_exact(&mut body).unwrap();
            let string_body = String::from_utf8(body).unwrap();
            Some(string_body)
        };

        let cookies = headers.remove("set-cookie");

        let cookies = if let Some(cookies) = cookies {
            let cookies = cookies.split(";").collect::<Vec<&str>>();
            cookies
                .into_iter()
                .filter_map(|cookie| {
                    cookie
                        .split_once('=')
                        .map(|(name, value)| (name.to_string(), value.to_string()))
                })
                .collect::<HashMap<String, String>>()
        } else {
            HashMap::new()
        };

        let cookies = CookieJar::new(cookies);

        let res = Response {
            http_version,
            status_code,
            headers,
            body,
            cookies,
        };

        res
    }
}
