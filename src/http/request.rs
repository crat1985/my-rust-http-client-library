use std::{collections::HashMap, io::Write, net::TcpStream};

use super::{cookies::CookieJar, http_version::HttpVersion, method::Method, response::Response};

#[derive(Debug)]
pub struct Request {
    addr: String,
    method: Method,
    uri: String,
    http_version: HttpVersion,
    headers: HashMap<String, String>,
    body: Option<String>,
}

impl Request {
    pub(crate) fn new(method: Method, addr: impl Into<String>, uri: impl Into<String>) -> Self {
        Self {
            addr: addr.into(),
            method,
            uri: uri.into(),
            http_version: HttpVersion::OneDotOne,
            headers: HashMap::new(),
            body: None,
        }
    }

    pub fn headers_mut(&mut self) -> &mut HashMap<String, String> {
        &mut self.headers
    }

    pub fn set_cookies(&mut self, cookies: CookieJar) {
        let cookies = cookies
            .into_inner()
            .into_iter()
            .map(|(name, value)| format!("{name}={value}"))
            .collect::<Vec<String>>()
            .join(";");
        self.headers.insert("cookie".to_string(), cookies);
    }

    pub fn json_body(&mut self, body: &str) {
        self.body = Some(body.to_string());
        self.headers.insert("content-type".to_string(), "application/json".to_string());
        self.headers.insert("content-length".to_string(), body.len().to_string());
    }

    pub fn plain_text_body(&mut self, body: &str) {
        self.body = Some(body.to_string());
        self.headers.insert("content-type".to_string(), "text/plain".to_string());
        self.headers.insert("content-length".to_string(), body.len().to_string());
    }

    pub fn send(self) -> Response {
        let mut req = format!("{} {} {}\r\n", self.method, self.uri, self.http_version);
        for (name, value) in self.headers {
            req += &format!("{}: {}\r\n", name, value);
        }
        let req = req + "\r\n";
        let req = if let Some(body) = self.body {
            req + &format!("{body}\r\n")
        } else {
            req + "\r\n"
        };

        let mut stream = TcpStream::connect(self.addr).unwrap();

        stream.write(req.as_bytes()).unwrap();

        let res = Response::parse(&mut stream);

        res
    }
}
