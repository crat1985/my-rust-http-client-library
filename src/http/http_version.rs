#[derive(Debug)]
pub enum HttpVersion {
    OneDotOne,
}

impl std::fmt::Display for HttpVersion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::OneDotOne => write!(f, "HTTP/1.1"),
        }
    }
}

impl HttpVersion {
    pub(crate) fn parse(value: &str) -> Self {
        match value.to_lowercase().as_str() {
            "http/1.1" => Self::OneDotOne,
            _ => todo!(),
        }
    }
}
