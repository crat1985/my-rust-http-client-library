use std::collections::HashMap;

#[derive(Debug)]
pub struct CookieJar {
    cookies: HashMap<String, String>,
}

impl CookieJar {
    pub fn new(cookies: HashMap<String, String>) -> Self {
        Self { cookies }
    }

    pub fn insert(&mut self, name: &str, value: &str) {
        self.cookies.insert(name.to_string(), value.to_string());
    }

    pub fn get(&self, name: &str) -> Option<&String> {
        self.cookies.get(name)
    }

    pub fn remove(&mut self, name: &str) -> Option<String> {
        self.cookies.remove(name)
    }

    pub fn into_inner(self) -> HashMap<String, String> {
        self.cookies
    }
}
