pub mod http;

use http::request::Request;

use crate::http::method::Method;

pub struct HTTPClient {
    addr: String,
}

impl HTTPClient {
    pub fn new(addr: impl Into<String>) -> Self {
        Self { addr: addr.into() }
    }

    pub fn get(&self, uri: impl Into<String>) -> Request {
        Request::new(Method::Get, &self.addr, uri)
    }

    pub fn post(&self, uri: impl Into<String>) -> Request {
        Request::new(Method::Post, &self.addr, uri)
    }

    pub fn put(&self, uri: impl Into<String>) -> Request {
        Request::new(Method::Put, &self.addr, uri)
    }

    pub fn patch(&self, uri: impl Into<String>) -> Request {
        Request::new(Method::Patch, &self.addr, uri)
    }

    pub fn delete(&self, uri: impl Into<String>) -> Request {
        Request::new(Method::Delete, &self.addr, uri)
    }

    pub fn options(&self, uri: impl Into<String>) -> Request {
        Request::new(Method::Options, &self.addr, uri)
    }

    pub fn ws(&self, uri: impl Into<String>) -> Request {
        let mut req = Request::new(Method::Get, &self.addr, uri);
        let headers = req.headers_mut();
        headers.insert("upgrade".to_string(), "websocket".to_string());
        headers.insert("connection".to_string(), "upgrade".to_string());
        headers.insert("sec-websocket-key".to_string(), "AQIDBAUGBwgJCgsMDQ4PEC==".to_string());
        headers.insert("sec-websocket-version".to_string(), "13".to_string());
        
        req
    }
}
